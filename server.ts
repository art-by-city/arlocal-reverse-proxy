/// <reference types="./arlocal" />
import ArLocal from 'arlocal'
import HttpProxy from 'http-proxy'
import express, { Response } from 'express'
import cron from 'node-cron'
import fetch from 'node-fetch'
import cors from 'cors'
import dotenv from 'dotenv'

dotenv.config()

const host = process.env.HOST || 'localhost'
const ARLOCAL_PORT = process.env.ARLOCAL_PORT
  ? Number.parseInt(process.env.ARLOCAL_PORT)
  : 1984
const port = process.env.PORT
  ? Number.parseInt(process.env.PORT)
  : 1987
const DB_PATH = process.env.DB_PATH || undefined
const LOGGING = process.env.LOGGING || false
const origin = process.env.CORS_ORIGIN || 'http://localhost:3000'
const miningSchedule = process.env.CRON_MINING_SCHEDULE
const pricePerByte = process.env.PRICE_PER_BYTE
  ? Number.parseInt(process.env.PRICE_PER_BYTE)
  : false

const confirmations: { [key: string]: number } = {}

const startServer = async () => {
  // Setup ArLocal
  const arLocal = new ArLocal(ARLOCAL_PORT, LOGGING, DB_PATH)
  await arLocal.start()

  // Setup ArLocal Proxy
  const arlocalProxy = HttpProxy.createProxyServer({
    target: {
      protocol: 'http',
      host,
      port: ARLOCAL_PORT
    }
  })

  // Create express app
  const app = express()

  // Enable CORS
  app.use(cors({ origin }))

  // Disable /mining endpoints to arlocal
  function send404(res: Response) {
    res.status(404)
    res.send()
  }
  app.use('/mine', (req, res) => {
    send404(res)
  })
  app.use('/mine/:n', (req, res) => {
    send404(res)
  })

  // Allow custom price endpoint logic
  if (pricePerByte) {
    app.use('/price/:bytes/:addy?', (req, res) => {
      const bytes = Number.parseInt(req.params.bytes)

      if (Number.isNaN(bytes)) {
        res.status(400)
        return res.send({
          status: 400,
          error: 'size_must_be_an_integer'
        })
      }

      return res.send((bytes * pricePerByte).toString())
    })
  }

  // Wire proxy to ArLocal
  app.use((req, res) => {
    const requestOrigin = req.headers['origin']
    if (typeof requestOrigin === 'string' && requestOrigin === origin) {
      arlocalProxy.web(req, res)
    } else {
      send404(res)
    }
  })

  // Mining cron task
  if (miningSchedule) {
    cron.schedule('* * * * *', async () => {
      try {
        const res = await fetch(`http://${host}:${ARLOCAL_PORT}/mine`)
        if (LOGGING) {
          console.log('mining result', await res.json())
        }
      } catch (error) {
        console.error('Error calling /mine endpoint', error)
      }
    })
  }

  // Start app
  app.listen(port, () => {
    console.log(`arlocal proxy started on port ${port}`)
  })
}

(async () => {
  await startServer()
})()
