ArLocal Reverse Proxy

Usage:
```
$ npm i
$ npm start
```

Setup:

Proxy / Emulation Settings
- `HOST`
  - default: `localhost`
- `PORT`
  - default: `1987`
- `LOGGING`
  - enables logging of everything, including ArLocal and mining results
  - default: `false`
- `CORS_ORIGIN`
  - Only allow requests from this origin
  - default: `http://localhost:3000`
- `CRON_MINING_SCHEDULE`
  - Optionally enable cron-based calls to the `/mine` endpoint to simulate
    network activity
- `PRICE_PER_BYTE`
  - Optionally override ArLocal's `/price` endpoint with a custom price per byte

ArLocal Settings
- `ARLOCAL_PORT`
  - port to start ArLocal on
  - default: `1984`
- `DB_PATH`
  - path ArLocal should store temp db in
  - default: `undefined`
